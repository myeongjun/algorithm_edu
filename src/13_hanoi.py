def hanoi(n, from_pos, aux_pos, to_pos):
    if n == 1:
        print(from_pos, " -> ", to_pos)
        return

    hanoi(n-1, from_pos, to_pos, aux_pos)
    print(from_pos, " -> ", to_pos)
    hanoi(n-1, aux_pos, from_pos, to_pos)

if __name__ == "__main__":
    # print(hanoi(1, 1, 2, 3))
    hanoi(3, 1, 2, 3)
