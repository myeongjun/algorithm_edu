class Solution():
    def my_sum(self, n: int) -> int:
        sum_value = 0

        for i in range(0, n):
            sum_value += (i+1)
        
        return sum_value

    def my_sum2(self, n: int) -> int:
        sum_value = n*(n+1) // 2
        
        return sum_value


if __name__ == "__main__":
    ret = Solution().my_sum2(100)
    print(ret)
