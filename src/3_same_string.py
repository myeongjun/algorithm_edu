from typing import List

class Solution():
    def same_string(self, lists: List) -> str:
        result = set()

        for idx, val in enumerate(lists):
            for j in lists[idx+1:]:
                if val == j:
                    result.add(val)

        return result


if __name__ == "__main__":
    name = ["Tom", "Jerry", "Mike", "Tom"]
    ret = Solution().same_string(name)

    print(ret)
