def merge_sort(li):
    n = len(li)
    if n <= 1:
        return li

    mid = n//2

    g1 = merge_sort(li[:mid])
    g2 = merge_sort(li[mid:])

    result = []

    while g1 and g2:
        if g1[0] < g2[0]:
            result.append(g1.pop(0))
        else:
            result.append(g2.pop(0))

    while g1:
        result.append(g1.pop(0))

    while g2:
        result.append(g2.pop(0))

    return result


if __name__ == "__main__":
    li = [6, 8, 3, 9, 10, 1, 2, 4, 7, 5]
    print(merge_sort(li))
