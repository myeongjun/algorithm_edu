def find_friend(graph, name):
    qu = []
    done = set()

    qu.append((name, 0))
    done.add(name)

    while qu:
        (p, d) = qu.pop(0)
        print(p, d)

        for x in graph[p]:
            if x not in done:
                qu.append((x, d+1))
                done.add(x)


if __name__ == "__main__":
    fr_info = {
        'Summer': ['John', 'Justin', 'Mike'],
        'John': ['Summer', 'Justin'],
        'Justin': ['John', 'Summer', 'Mike', 'May'],
        'Mike': ['Summer', 'Justin'],
        'May': ['Justin', 'Kim'],
        'Kim': ['May'],
        'Tom': ['Jerry'],
        'Jerry': ['Tom'],
    }

    find_friend(fr_info, 'Summer')