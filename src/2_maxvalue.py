from typing import List

class Solution():
    def max_value(self, lists: List) -> int:
        max_val = lists[0]

        for i in lists:
            if max_val < i:
                max_val = i

        return max_val


if __name__ == "__main__":
    my_list = [17, 92, 18, 33, 58, 7, 33, 42]
    ret = Solution().max_value(my_list)

    print(ret)
