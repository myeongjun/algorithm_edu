import string

def solution(n):
    # Write your code here
    alphabet = list(string.ascii_uppercase)
    column = alphabet + list([i+j for i in alphabet for j in alphabet])

    q, r = divmod(n, len(column))

    if n % len(column) == 0:
        q -= 1

    print(str(q+1) + column[r-1])

solution(27)
