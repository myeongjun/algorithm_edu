def ins_sort(lists):
    length = len(lists)

    for i in range(1, length):
        key = lists[i]
        j = i-1

        while j >= 0 and lists[j] > key:
            lists[j+1] = lists[j]
            j -= 1
        
        lists[j+1] = key

if __name__ == "__main__":
    lists = [2, 4, 5, 1, 3]
    ins_sort(lists)
    print(lists)
