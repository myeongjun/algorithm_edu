import string

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def solution(s):
    # Creating printer
    alphabet = list(string.ascii_uppercase)
    root = current = prev = TreeNode(None)

    for alph in alphabet:
        current.left = TreeNode(alph)
        current = current.left
        current.right = prev
        prev = current

    current.left = root.left
    current = current.left
    current.right = prev

    # Finding character
    result = 0
    left = right = current
    for ch in s:
        left_val = 0
        right_val = 0

        while left:
            if left.val == ch:
                break
            left_val += 1
            left = left.left

        while right:
            if right.val == ch:
                break
            right_val += 1
            right = right.right
        
        if left_val < right_val:
            result += left_val
            current = right = left
        else:
            result += right_val
            current = left = right

    return result


print(solution("BZA"))
# print(solution("ABC"))
# print(solution("VGXGPUAMKX"))
