#include <stdio.h>

int main() {
    int arr[] = {17, 92, 18, 33, 58, 7, 33, 42};
    int target = 33;
    int len = sizeof(arr) / sizeof(arr[0]);

    for(int i = 0; i < len; i++){
        if(arr[i] == target){
            printf("%d", arr[i]);
            return i;
        }

    }

    return -1;
}
