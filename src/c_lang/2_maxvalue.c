#include <stdio.h>

int main() {
    int arr[] = {17, 92, 18, 33, 58, 7, 33, 42};
    int len = sizeof(arr) / sizeof(arr[0]);
    int max_val = arr[0];

    for(int i = 0; i < len; i++) {
        if(max_val < arr[i]) {
            max_val = arr[i];
        }
    }

    printf("%d", max_val);
    return 0;
}
