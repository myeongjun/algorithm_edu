#include <stdio.h>

int main() {
    int arr[] = {1, 3, 5, 7, 9};
    int len = sizeof(arr) / sizeof(arr[0]);
    int target = 7;
    
    int first = 0;
    int last = len-1;
    int mid;
    
    while(first <= last) {
        mid = (first+last) / 2;
        
        if(target == arr[mid]) {
            return mid;
        } else {
            if(target < arr[mid])
                last = mid-1;
            else
                first = mid+1;
        }
    }
    
    return -1;
}
