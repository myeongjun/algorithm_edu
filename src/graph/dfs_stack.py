def stack_dfs(start_v):
    graph = {
        1: [2, 3, 4],
        2: [5],
        3: [5],
        4: [],
        5: [6, 7],
        6: [],
        7: [3]
    }
    discovered = []
    stack =[start_v]
    while stack:
        v = stack.pop()
        if v not in discovered:
            discovered.append(v)
            for w in graph[v]:
                stack.append(w)

    return discovered

if __name__ == "__main__":
    # 1 4 3 5 7 6 2
    print(stack_dfs(1))
