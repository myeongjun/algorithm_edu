from typing import List

class Solution():
    def same_string(self, lists: List) -> str:
        name_dic = {}
        for name in lists:
            if name in name_dic:
                name_dic[name] += 1
            else:
                name_dic[name] = 1

        result = set()
        for name in name_dic:
            if name_dic[name] >= 2:
                result.add(name)
        
        return result


if __name__ == "__main__":
    name = ["Tom", "Jerry", "Mike", "Tom"]
    ret = Solution().same_string(name)

    print(ret)
