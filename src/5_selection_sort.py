def sel_sort(n):
    length = len(n)

    for i in range(0, length-1):
        min_idx = i

        for j in range(i+1, length):
            if n[j] < n[min_idx]:
                min_idx = j

        n[i], n[min_idx] = n[min_idx], n[i]

if __name__ == "__main__":
    n = [3, 4, 2, 1]
    sel_sort(n)
    print(n)
