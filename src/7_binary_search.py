def binary_search(a, target):
    start = 0
    end = len(a) - 1

    while start <= end:
        mid = (start + end) // 2

        if target == a[mid]:
            return mid

        elif target > a[mid]:
            start = mid + 1

        else:
            end = mid - 1

    return -1

if __name__ == "__main__":
    my_list = [1, 4, 5, 6, 7, 8, 9, 10]
    print(binary_search(my_list, 6))
